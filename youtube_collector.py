#!./python/bin/python3
import time
import datetime
import pymysql
import feedparser
from dateutil import tz
import xml.etree.ElementTree as etree
from pytube import YouTube
from pprint import pprint as pprint


def central_time(time_string):
    '''Youtube presents time in UTC, I'd prefer Central, but I will also check
    to see how old a video is and if it should be downloaded and pass that
    back as well'''
    #Check to see how old a video is
    time_string = time_string.split("+")[0]
    date_time_object = datetime.datetime.strptime(time_string, "%Y-%m-%dT%H:%M:%S")
    difference = datetime.datetime.now() - date_time_object
    dl_decision = False
    if difference <= datetime.timedelta(hours=24):
        dl_decision = True
    #begin conversion
    from_zone = tz.gettz('UTC')
    to_zone = tz.gettz('America/Central')
    date_time_object = date_time_object.replace(tzinfo=from_zone)
    corrected_time_object = date_time_object.astimezone(to_zone)
    date_string = datetime.datetime.strftime(corrected_time_object, "%Y/%m/%d")
    time_string = datetime.datetime.strftime(corrected_time_object, "%H:%M:%S")
    return date_string, time_string, dl_decision


def connect_database():
    """Simply connects to the database"""
    db = pymysql.connect("youdbinternal","root","gudpassword")
    db.autocommit(True)
    cursor = db.cursor()
    try:
        cursor.execute("USE youdl")
    except pymysql.err.InternalError:
        print("Database youdl doesn't exist, creating...")
        init_database(cursor)

    return cursor, db


def init_database(cursor):
    """Creates the database and the videos table if they don't already exist"""
    cursor.execute("CREATE DATABASE IF NOT EXISTS youdl")
    cursor.execute("USE youdl")
    cursor.execute("SHOW TABLES")
    db_tables = cursor.fetchall()
    if ('videos', ) not in db_tables:
        cursor.execute("USE youdl")
        cursor.execute("""CREATE TABLE videos(
        video_id INT NOT NULL AUTO_INCREMENT,
        video_author VARCHAR(400) NOT NULL,
        video_title VARCHAR(400) NOT NULL,
        video_link VARCHAR(400) NOT NULL,
        video_summary VARCHAR(10000) NOT NULL,
        video_published_date DATE NOT NULL,
        video_published_time TIME NOT NULL,
        video_youid varchar(50) NOT NULL,
        video_downloaded varchar(5) NOT NULL DEFAULT 'NO',
        CHECK (video_downloaded IN ('YES','NO','ERROR', 'STALE')),
        PRIMARY KEY ( video_id ))""")
        cursor.execute("ALTER TABLE videos CONVERT TO CHARACTER SET utf8mb4")


def cln(instring):
    """Attempts to remove characters that SQL doesn't like from strings
    returned from pytube"""
    badstrings = '()}{<>/|-?!:+*%^&@;\"\''
    for c in badstrings:
        newstring = instring.replace(c, "")
    return newstring


def youtube_collector():
    """Adds any new youtube videos it finds to the database"""
    cursor, db = connect_database()
    errors = {}
    sub_file = open("/youtube_dl/subscription_manager", "r")
    xmldoc = etree.parse(sub_file)
    root = xmldoc.getroot()
    sublist = []
    badstrings = '()}{<>/[]|-?!:+*%^&@;\"\''
    for child in root[0][0]:
        feed = child.attrib['xmlUrl']
        sublist.append(feed)
    for feedaddress in sublist:
        d = feedparser.parse(feedaddress)
        for entry in d['entries']:
            #check that entry doesn't already exist
            youid = entry['yt_videoid']
            cursor.execute("SELECT video_youid FROM videos WHERE video_youid=\"{}\"".format(youid))
            youid_check = cursor.fetchall()
            if (youid,) not in youid_check:
                publish_date_time = entry['published']
                publish_date, publish_time, dl_decision = central_time(publish_date_time)
                #mark videos older than 24 hours as stale to prevent re-downloading too many videos if pod goes down
                if dl_decision:
                    video_downloaded = "NO"
                else:
                    video_downloaded = "STALE"
                summary = entry['summary']
                author = d['feed']['title']
                title = entry['title']
                link = entry['link']
                try:
                    cursor.execute("""INSERT INTO videos SET
                    video_published_date='{}', video_summary='{}', video_author='{}',
                    video_title='{}', video_link='{}', video_youid='{}',
                    video_published_time='{}', video_downloaded='{}'""".format(publish_date, cln(summary), cln(author),
                    cln(title), link, youid, publish_time, video_downloaded))
                except Exception as e:
                    errors[youid] = e

    cursor.execute("""select video_author, video_title, video_published_date,
    video_published_time, video_downloaded from videos order by
    video_published_date desc, video_published_time desc limit 20;""")
    results = cursor.fetchall()
    #print the 20 most recent entries, this helps add some visibility to how the
    #container is running.
    for row in results:
        for col in row:
            print("{},".format(col), end="")
        print("")

    pprint(errors)
    print("\nLast RUN: {}\n--------------------------------\n".format(datetime.datetime.now()))
    db.commit()
    db.close()


def main():
    while True:
        youtube_collector()
        time.sleep(60) #Sleep for 10 minutes

if __name__ == '__main__':
    main()
